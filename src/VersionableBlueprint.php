<?php

namespace Thunderwolf\EloquentVersionable;

use Illuminate\Database\Schema\Blueprint;

class VersionableBlueprint
{
    /**
     * Add sluggable column to the table. Also create an index.
     *
     * @param Blueprint $table
     * @param array $columns
     * @param string $pk_name
     * @throws VersionableException
     */
    public static function createVersionableColumns(Blueprint $table, array $columns, string $pk_name = 'id')
    {
        $columns = VersionableHelper::parseColumns($columns);
        self::createStandardColumns($table, $columns);
        $table->index([$columns['version_column'], $pk_name]);
    }

    /**
     * Add sluggable column to the table. Also create an index.
     *
     * @param Blueprint $table
     * @param array $columns
     * @throws VersionableException
     */
    public static function createVersionableVersionColumns(Blueprint $table, array $columns)
    {
        $columns = VersionableVersionHelper::parseColumns($columns);
//        $table->increments('id');
//        $table->unsignedInteger('version_id');
        $table->unsignedInteger('id');
        self::createStandardColumns($table, $columns);
        $obj = new $columns['versionable_model'];
//        $table->foreign('version_id')->references($columns['versionable_model_foreign_key'])->on($obj->getTable());
        $table->foreign('id')->references($columns['versionable_model_foreign_key'])->on($obj->getTable());
//        $table->index([$columns['version_column'], 'version_id']);
        $table->index([$columns['version_column'], 'id']);
    }

    /**
     * @param Blueprint $table
     * @param array $columns
     * @return void
     */
    private static function createStandardColumns(Blueprint $table, array $columns)
    {
        $table->unsignedInteger($columns['version_column'])->default(0);
        if ($columns['log_created_at']) {
            $table->dateTimeTz($columns['version_created_at_column']);
        }
        if ($columns['log_created_by']) {
            $table->string($columns['version_created_by_column'], 100);
        }
        if ($columns['log_comment']) {
            $table->string($columns['version_comment_column'], 255);
        }
    }

    /**
     * Drop sluggable column and index.
     *
     * @param Blueprint $table
     * @param array $columns
     * @param string $pk_name
     * @throws VersionableException
     */
    public static function dropVersionableColumns(Blueprint $table, array $columns, string $pk_name = 'id')
    {
        $columns = VersionableHelper::parseColumns($columns);
        $table->dropIndex([$columns['version_column'], $pk_name]);
        self::dropStandardColumns($table. $columns);
    }

    public static function dropVersionableVersionColumns(Blueprint $table, array $columns)
    {
        $columns = VersionableVersionHelper::parseColumns($columns);
//        $table->dropForeign('version_id');
        $table->dropForeign('id');
//        $table->dropIndex([$columns['version_column'], 'version_id']);
        $table->dropIndex([$columns['version_column'], 'id']);
        self::dropStandardColumns($table. $columns);
    }

    /**
     * @param Blueprint $table
     * @param array $columns
     * @return void
     */
    private static function dropStandardColumns(Blueprint $table, array $columns)
    {
        $table->dropColumn($columns['version_column']);
        if ($columns['log_created_at']) {
            $table->dropColumn($columns['version_created_at_column']);
        }
        if ($columns['log_created_by']) {
            $table->dropColumn($columns['version_created_by_column']);
        }
        if ($columns['log_comment']) {
            $table->dropColumn($columns['version_comment_column']);
        }
    }

}