<?php

namespace Thunderwolf\EloquentVersionable;

class VersionableHelper
{
    /**
     * The name of default version column.
     */
    const VersionColumn = 'version';

    /**
     * The name of default version_created_at column.
     */
    const VersionCreatedAtColumn = 'version_created_at';

    /**
     * The name of default version_created_by column.
     */
    const VersionCreatedByColumn = 'version_created_by';

    /**
     * The name of default version_created_at column.
     */
    const VersionCommentColumn = 'version_comment';

    /**
     * Parsing columns given to
     *
     * @param array $columns
     * @return array
     * @throws VersionableException
     */
    public static function parseColumns(array $columns): array
    {
        if (!array_key_exists('version_model', $columns)) {
            throw new VersionableException('Setting version_model is Required to save versions');
        }
        if (!array_key_exists('version_column', $columns)) {
            $columns['version_column'] = self::VersionColumn;
        }
        if (!array_key_exists('log_created_at', $columns)) {
            $columns['log_created_at'] = false;
        }
        if (!array_key_exists('version_created_at_column', $columns)) {
            $columns['version_created_at_column'] = self::VersionCreatedAtColumn;
        } else {
            if (!$columns['log_created_at']) {
                throw new VersionableException('Setting version_created_at_column without enabling log_created_at');
            }
        }
        if (!array_key_exists('log_created_by', $columns)) {
            $columns['log_created_by'] = false;
        }
        if (!array_key_exists('version_created_by_column', $columns)) {
            $columns['version_created_by_column'] = self::VersionCreatedByColumn;
        } else {
            if (!$columns['log_created_by']) {
                throw new VersionableException('Setting version_created_by_column without enabling log_created_by');
            }
        }
        if (!array_key_exists('log_comment', $columns)) {
            $columns['log_comment'] = false;
        }
        if (!array_key_exists('version_comment_column', $columns)) {
            $columns['version_comment_column'] = self::VersionCommentColumn;
        } else {
            if (!$columns['log_created_by']) {
                throw new VersionableException('Setting version_comment_column without enabling log_comment');
            }
        }
        return $columns;
    }
}