<?php

namespace Thunderwolf\EloquentVersionable;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Date;

trait VersionableVersion
{

    /**
     * Generated Model configuration cache
     *
     * @var array
     */
    public array $versionableVersionConfiguration = [];

    /**
     * Configuration for the VersionableVersion Trait
     *
     * @return array Array in the format VersionableVersionHelper::COLUMNS
     */
    abstract public static function versionableVersion(): array;

    /**
     * Process configuration only once per object
     *
     * @return array
     * @throws VersionableException
     */
    public function getVersionableVersionConfiguration(): array
    {
        if (empty($this->versionableVersionConfiguration)) {
            $this->versionableVersionConfiguration = VersionableVersionHelper::parseColumns(
                get_called_class()::versionableVersion()
            );
        }
        return $this->versionableVersionConfiguration;
    }

    /**
     * Get the [version_columns] configuration value.
     *
     * @return array
     * @throws VersionableException
     */
    public function getVersionableColumns(): array
    {
        return $this->getVersionableVersionConfiguration()['version_columns'];
    }

    /**
     * Get the [versionable_model] configuration value.
     *
     * @return string
     * @throws VersionableException
     */
    public function getVersionableModelName(): string
    {
        return $this->getVersionableVersionConfiguration()['versionable_model'];
    }

    /**
     * Get the [versionable_model_foreign_key] configuration value.
     *
     * @return string
     * @throws VersionableException
     */
    public function getVersionableModelForeignKey(): string
    {
        return $this->getVersionableVersionConfiguration()['versionable_model_foreign_key'];
    }


    /**
     * Get the [version_column] column key name.
     *
     * @return string
     * @throws VersionableException
     */
    public function getVersionColumnName(): string
    {
        return $this->getVersionableVersionConfiguration()['version_column'];
    }

    /**
     * Get the [version_column] column value.
     *
     * @return int
     * @throws VersionableException
     */
    public function getVersion(): int
    {
        return $this->getAttributeValue($this->getVersionColumnName());
    }

    /**
     * Set the [version_column] column value.
     *
     * @param int $version
     * @return void
     * @throws VersionableException
     */
    public function setVersion(int $version): void
    {
        $this->setAttribute($this->getVersionColumnName(), $version);
    }

    /**
     * Get the [log_created_at] configuration value.
     *
     * @return bool
     * @throws VersionableException
     */
    public function isVersionCreatedAtUsed(): bool
    {
        return !!$this->getVersionableVersionConfiguration()['log_created_at'];

    }

    /**
     * Get the [version_created_at_column] column key name.
     *
     * @return string
     * @throws VersionableException
     */
    public function getVersionCreatedAtColumnName(): string
    {
        if (!$this->isVersionCreatedAtUsed()) {
            throw new VersionableException('log_created_at is set to false');
        }
        return $this->getVersionableVersionConfiguration()['version_created_at_column'];
    }

    /**
     * Get the [version_created_at_column] column value.
     *
     * @return Carbon
     * @throws VersionableException
     */
    public function getVersionCreatedAt(): Carbon
    {
        return $this->asDateTime($this->getAttributeValue($this->getVersionCreatedAtColumnName()));
    }

    /**
     * Set the [version_created_at_column] column value.
     *
     * @param Carbon $time
     * @return void
     * @throws VersionableException
     */
    public function setVersionCreatedAt(Carbon $time): void
    {
        $this->setAttribute($this->getVersionCreatedAtColumnName(), $time);
    }

    /**
     * Get the [log_created_by] configuration value.
     *
     * @return bool
     * @throws VersionableException
     */
    public function isVersionCreatedByUsed(): bool
    {
        return !!$this->getVersionableVersionConfiguration()['log_created_by'];

    }

    /**
     * Get the [version_created_by_column] column key name.
     *
     * @return string|null
     * @throws VersionableException
     */
    public function getVersionCreatedByColumnName(): ?string
    {
        if (!$this->isVersionCreatedByUsed()) {
            throw new VersionableException('log_created_by is set to false');
        }
        return $this->getVersionableVersionConfiguration()['version_created_by_column'];
    }

    /**
     * Get the [version_created_by_column] column value.
     *
     * @return mixed
     * @throws VersionableException
     */
    public function getVersionCreatedBy()
    {
        return $this->getAttributeValue($this->getVersionCreatedByColumnName());
    }

    /**
     * Set the [version_created_by_column] column value.
     *
     * @param mixed $user
     * @return void
     * @throws VersionableException
     */
    public function setVersionCreatedBy($user): void
    {
        $this->setAttribute($this->getVersionCreatedByColumnName(), $user);
    }

    /**
     * Get the [log_comment] configuration value.
     *
     * @return bool
     * @throws VersionableException
     */
    public function isVersionCommentUsed(): bool
    {
        return !!$this->getVersionableVersionConfiguration()['log_comment'];

    }

    /**
     * Get the [version_comment_column] column key name.
     *
     * @return string
     * @throws VersionableException
     */
    public function getVersionCommentColumnName(): string
    {
        if (!$this->isVersionCommentUsed()) {
            throw new VersionableException('log_comment is set to false');
        }
        return $this->getVersionableVersionConfiguration()['version_comment_column'];
    }

    /**
     * Get the [version_comment_column] column value.
     *
     * @return string|null
     * @throws VersionableException
     */
    public function getVersionComment(): ?string
    {
        return $this->getAttributeValue($this->getVersionCommentColumnName());
    }

    /**
     * Set the [version_comment_column] column value.
     *
     * @param string|null $comment
     * @return void
     * @throws VersionableException
     */
    public function setVersionComment(?string $comment): void
    {
        $this->setAttribute($this->getVersionCommentColumnName(), $comment);
    }

    /**
     * Get the versionable relation.
     * @throws VersionableException
     */
    public function versionableParent(): BelongsTo
    {
        return $this->belongsTo($this->getVersionableModelName());
    }

    public function getIgnoredAttributes(): array
    {
        $ignored = [$this->getVersionColumnName()];
        if ($this->isVersionCreatedAtUsed()) {
            $ignored[] = $this->getVersionCreatedAtColumnName();
        }
        if ($this->isVersionCreatedByUsed()) {
            $ignored[] = $this->getVersionCreatedByColumnName();
        }
        return $ignored;
    }
}