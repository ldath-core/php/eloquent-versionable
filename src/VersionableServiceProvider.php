<?php

namespace Thunderwolf\EloquentVersionable;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\ServiceProvider;

class VersionableServiceProvider extends ServiceProvider
{
    public function register()
    {
        Blueprint::macro('createVersionable', function (array $columns) {
            VersionableBlueprint::createVersionableColumns($this, $columns);
        });

        Blueprint::macro('createVersionableVersion', function (array $columns) {
            VersionableBlueprint::createVersionableVersionColumns($this, $columns);
        });

        Blueprint::macro('dropVersionable', function (array $columns) {
            VersionableBlueprint::dropVersionableColumns($this, $columns);
        });

        Blueprint::macro('dropVersionableVersion', function (array $columns) {
            VersionableBlueprint::dropVersionableVersionColumns($this, $columns);
        });
    }
}