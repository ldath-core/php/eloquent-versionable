<?php

namespace Thunderwolf\EloquentVersionable;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Date;
use Illuminate\Database\Eloquent\Builder;

trait Versionable
{
    /**
     * Whether the versioning is enabled
     *
     * @var bool
     */
    protected static bool $isVersioningEnabled = true;

    /**
     * Generated Model configuration cache
     *
     * @var array
     */
    public array $versionableConfiguration = [];

    /**
     * Enforce a new Version of this object upon next save if true.
     *
     * @var bool
     */
    protected bool $enforceVersion = false;

    /**
     * Create new Version when save done if true
     *
     * @var bool
     */
    protected bool $createVersion = false;

    /**
     * Deleting
     *
     * @var bool
     */
    protected bool $deleting = false;

    /**
     * An array of objects scheduled for deletion.
     *
     * @var Collection|null
     */
    protected ?Collection $versionsScheduledForDeletion = null;

    /**
     * Configuration for the Versionable Trait
     *
     * @return array Array in the format VersionableHelper::COLUMNS
     */
    abstract public static function versionable(): array;

    /**
     * Get the versions for the current Model.
     * @throws VersionableException
     */
    public function versions(): HasMany
    {
        return $this->hasMany($this->getVersionModelName());
    }

    /**
     * Booting Eloquent Sortable Trait
     *
     *  creating and created: sent before and after records have been created.
     *  updating and updated: sent before and after records are updated.
     *  saving and saved: sent before and after records are saved (i.e created or updated).
     *  deleting and deleted: sent before and after records are deleted or soft-deleted.
     *  restoring and restored: sent before and after soft-deleted records are restored.
     *  retrieved: sent after records have been retrieved.
     *
     * @return void
     * @throws VersionableException
     */
    public static function bootVersionable()
    {
        static::saving(function ($model) {
            if ($model->isVersioningNecessary()) {
                self::setVersionableColumns($model);
            }
        });

        static::deleting(function ($model) {
            $model->deleting = true; // TODO: not used at the moment probably for the softDelete in the fufutre
            if ($model->isVersioningNecessary()) {
                self::setVersionableColumns($model);
            }
            // TODO: at the moment we do not have soft delete let's just cleanup versions
            $model->deleteAllVersions();
        });

        static::saved(function ($model) {
            if ($model->createVersion) {
                $model->addVersion();
            }
        });
    }

    /**
     * Setting up defaults for Versionable columns if not set and enable create version
     *
     * @param self $model
     * @return void
     * @throws VersionableException
     */
    public static function setVersionableColumns(self $model): void
    {
        $model->setVersion(!$model->exists() ? 1 : $model->getLastVersionNumber() + 1);

        if ($model->isVersionCreatedAtUsed() && !$model->isDirty($model->getVersionCreatedAtColumnName())) {
            $model->setVersionCreatedAt(Date::now());
        }
        // TODO: In the Eloquent to have attribute dirty there must be a change - we need to rethink this block
//        if ($model->isVersionCreatedByUsed() && !$model->isDirty($model->getVersionCreatedByColumnName())) {
//            $model->setVersionCreatedBy(null); // Unknown user was creating this version
//        }
//        if ($model->isVersionCommentUsed() && !$model->isDirty($model->getVersionCommentColumnName())) {
//            $model->setVersionComment(null); // Comment was not added
//        }
        $model->createVersion = true; // for postSave hook
    }

    /**
     * Process configuration only once per object
     *
     * @return array
     * @throws VersionableException
     */
    public function getVersionableConfiguration(): array
    {
        if (empty($this->versionableConfiguration)) {
            $this->versionableConfiguration = VersionableHelper::parseColumns(get_called_class()::versionable());
        }
        return $this->versionableConfiguration;
    }

    // accessing methods

    /**
     * Get the [version_model] configuration value.
     *
     * @return string
     * @throws VersionableException
     */
    public function getVersionModelName(): string
    {
        return $this->getVersionableConfiguration()['version_model'];
    }

    /**
     * Get the [version_column] column key name.
     *
     * @return string
     * @throws VersionableException
     */
    public function getVersionColumnName(): string
    {
        return $this->getVersionableConfiguration()['version_column'];
    }

    /**
     * Get the [version_column] column value.
     *
     * @return int
     * @throws VersionableException
     */
    public function getVersion(): int
    {
        return $this->getAttributeValue($this->getVersionColumnName());
    }

    /**
     * Set the [version_column] column value.
     *
     * @param int $version
     * @return void
     * @throws VersionableException
     */
    public function setVersion(int $version): void
    {
        $this->setAttribute($this->getVersionColumnName(), $version);
    }

    /**
     * Get the [log_created_at] configuration value.
     *
     * @return bool
     * @throws VersionableException
     */
    public function isVersionCreatedAtUsed(): bool
    {
        return !!$this->getVersionableConfiguration()['log_created_at'];

    }

    /**
     * Get the [version_created_at_column] column key name.
     *
     * @return string
     * @throws VersionableException
     */
    public function getVersionCreatedAtColumnName(): string
    {
        if (!$this->isVersionCreatedAtUsed()) {
            throw new VersionableException('log_created_at is set to false');
        }
        return $this->getVersionableConfiguration()['version_created_at_column'];
    }

    /**
     * Get the [version_created_at_column] column value.
     *
     * @return Carbon
     * @throws VersionableException
     */
    public function getVersionCreatedAt(): Carbon
    {
        return $this->asDateTime($this->getAttributeValue($this->getVersionCreatedAtColumnName()));
    }

    /**
     * Set the [version_created_at_column] column value.
     *
     * @param Carbon $time
     * @return void
     * @throws VersionableException
     */
    public function setVersionCreatedAt(Carbon $time): void
    {
        $this->setAttribute($this->getVersionCreatedAtColumnName(), $time);
    }

    /**
     * Get the [log_created_by] configuration value.
     *
     * @return bool
     * @throws VersionableException
     */
    public function isVersionCreatedByUsed(): bool
    {
        return !!$this->getVersionableConfiguration()['log_created_by'];

    }

    /**
     * Get the [version_created_by_column] column key name.
     *
     * @return string|null
     * @throws VersionableException
     */
    public function getVersionCreatedByColumnName(): ?string
    {
        if (!$this->isVersionCreatedByUsed()) {
            throw new VersionableException('log_created_by is set to false');
        }
        return $this->getVersionableConfiguration()['version_created_by_column'];
    }

    /**
     * Get the [version_created_by_column] column value.
     *
     * @return mixed
     * @throws VersionableException
     */
    public function getVersionCreatedBy()
    {
        return $this->getAttributeValue($this->getVersionCreatedByColumnName());
    }

    /**
     * Set the [version_created_by_column] column value.
     *
     * @param mixed $user
     * @return void
     * @throws VersionableException
     */
    public function setVersionCreatedBy($user): void
    {
        $this->setAttribute($this->getVersionCreatedByColumnName(), $user);
    }

    /**
     * Get the [log_comment] configuration value.
     *
     * @return bool
     * @throws VersionableException
     */
    public function isVersionCommentUsed(): bool
    {
        return !!$this->getVersionableConfiguration()['log_comment'];

    }

    /**
     * Get the [version_comment_column] column key name.
     *
     * @return string
     * @throws VersionableException
     */
    public function getVersionCommentColumnName(): string
    {
        if (!$this->isVersionCommentUsed()) {
            throw new VersionableException('log_comment is set to false');
        }
        return $this->getVersionableConfiguration()['version_comment_column'];
    }

    /**
     * Get the [version_comment_column] column value.
     *
     * @return string|null
     * @throws VersionableException
     */
    public function getVersionComment(): ?string
    {
        return $this->getAttributeValue($this->getVersionCommentColumnName());
    }

    /**
     * Set the [version_comment_column] column value.
     *
     * @param string|null $comment
     * @return void
     * @throws VersionableException
     */
    public function setVersionComment(?string $comment): void
    {
        $this->setAttribute($this->getVersionCommentColumnName(), $comment);
    }

    // versionable behavior

    /**
     * Checks whether versioning is enabled
     *
     * @return boolean
     */
    public static function isVersioningEnabled(): bool
    {
        return self::$isVersioningEnabled;
    }

    /**
     * Enables versioning
     *
     * @return void
     */
    public static function enableVersioning(): void
    {
        self::$isVersioningEnabled = true;
    }

    /**
     * Disables versioning
     *
     * @return void
     */
    public static function disableVersioning(): void
    {
        self::$isVersioningEnabled = false;
    }

    /**
     * Enforce a new Version of this object upon next save.
     *
     * @return self
     */
    public function enforceVersioning(): self
    {
        $this->enforceVersion = true;

        return $this;
    }

    /**
     * Checks whether the current state must be recorded as a version
     *
     * @return bool
     */
    public function isVersioningNecessary(): bool
    {
        if ($this->alreadyInSave) {
            return false;
        }

        if ($this->enforceVersion) {
            return true;
        }

        if (self::isVersioningEnabled() && (!$this->exists() || $this->getDirty() || $this->deleting)) {
            return true;
        }

        return false;
    }

    /**
     * Creates a version of the current object and saves it.
     *
     * @return Model A version object
     * @throws VersionableException
     * @throws MassAssignmentException
     */
    public function addVersion(): Model
    {
        $this->enforceVersion = false;

        $object_name = $this->getVersionModelName();
//        var_dump($object_name);
        $target = new $object_name();
        $keys = $target->getVersionableColumns();
//        $target->setAttribute('version_id', $this->getKey());
        $target->setAttribute('id', $this->getKey());
        $version = self::fillVersionsData($keys, $this, $target);
        $version->setVersion($this->getVersion());

        $version->save();

        return $version;
    }

    /**
     * Sets the properties of the current object to the value they had at a specific version
     *
     * @param int $versionNumber The version number to read
     * @return self The current object (for fluent API support)
     * @throws VersionableException - if no object with the given version can be found.
     */
    public function toVersion(int $versionNumber): self
    {
        $version = $this->getOneVersion($versionNumber);
        if (!$version) {
            throw new VersionableException(sprintf('No object found with version %d', $versionNumber));
        }
        $this->populateFromVersion($version);

        return $this;
    }

    /**
     * Sets the properties of the current object to the value they had at a specific version
     *
     * @param   Model $version The version object to use
     * @param   array $loadedObjects objects that's been loaded in a chain of populateFromVersion calls on referrer or fk objects.
     * @return  self The current object (for fluent API support)
     * @throws VersionableException
     */
    public function populateFromVersion(Model $version, array &$loadedObjects = []): self
    {
        $loadedObjects[get_class($this)][$version->getKey()][$version->getVersion()] = $this;
        $keys = $version->getVersionableColumns();
        return self::fillVersionsData($keys, $version, $this);
    }

    /**
     * Fill target data based on the source data and keys we can use
     *
     * @param array $keys
     * @param Model $source
     * @param Model $target
     * @return Model
     */
    protected static function fillVersionsData(array $keys, Model $source, Model $target): Model
    {
        $attributes = $source->attributesToArray();
        $toFill = array_filter($attributes, function($key)use($keys){
            return in_array($key,$keys);
        }, ARRAY_FILTER_USE_KEY);

//        var_dump($toFill);

        foreach ($toFill as $key => $value) {
            $target->setAttribute($key, $value);
        }

        if ($target->isVersionCreatedAtUsed()) {
            $target->setVersionCreatedAt($source->getVersionCreatedAt());  // TODO: In the documentation this must be documented
        }
        if ($target->isVersionCreatedByUsed()) {
            $target->setVersionCreatedBy($source->getVersionCreatedBy());  // TODO: In the documentation this must be documented
        }
        if ($target->isVersionCommentUsed()) {
            $target->setVersionComment($source->getVersionComment());  // TODO: In the documentation this must be documented
        }
        return $target;
    }

    /**
     * Gets the latest persisted version number for the current object
     *
     * @return  int
     * @throws VersionableException
     */
    public function getLastVersionNumber(): int
    {
        $object_name = $this->getVersionModelName();
        $version_obj = new $object_name();

        $v = $object_name::where($version_obj->getVersionableModelForeignKey(), $this->getKey())
            ->orderBy($version_obj->getVersionColumnName(), 'desc')
            ->first();
        unset($version_obj);
        if (!$v) {
            return 0;
        }

        return $v->getVersion();
    }

    /**
     * Checks whether the current object is the latest one
     *
     * @return bool
     * @throws VersionableException
     */
    public function isLastVersion(): bool
    {
        return $this->getLastVersionNumber() == $this->getVersion();
    }

    /**
     * Retrieves a version object for this entity and a version number
     *
     * @param int $versionNumber The version number to read
     * @return Model|null A version object
     * @throws VersionableException
     */
    public function getOneVersion(int $versionNumber): ?Model
    {
        $object_name = $this->getVersionModelName();
        $version_obj = new $object_name();

        $v = $object_name::where($version_obj->getVersionableModelForeignKey(), $this->getKey())
            ->where($version_obj->getVersionColumnName(), $versionNumber)
            ->first();
        unset($version_obj);
        return $v;
    }

    /**
     * Gets all the versions of this object, in incremental order
     *
     * @return Collection A list of twPageVersion objects
     * @throws VersionableException
     */
    public function getAllVersions(): Collection
    {
        $object_name = $this->getVersionModelName();
        $version_obj = new $object_name();

        $v = $object_name::where($version_obj->getVersionableModelForeignKey(), $this->getKey())
            ->orderBy($version_obj->getVersionColumnName());
        unset($version_obj);
        return $v->get();
    }

    protected function deleteAllVersions(): int
    {
        $object_name = $this->getVersionModelName();
        $version_obj = new $object_name();

        $v = $object_name::where($version_obj->getVersionableModelForeignKey(), $this->getKey())
            ->orderBy($version_obj->getVersionColumnName());
        unset($version_obj);
        return $v->delete();
    }

    /**
     * Compares the current object with another of its version.
     * <code>
     * print_r($book->compareVersion(1));
     * => array(
     *   '1' => array('Title' => 'Book title at version 1'),
     *   '2' => array('Title' => 'Book title at version 2')
     * );
     * </code>
     *
     * @param int $versionNumber
     * @param string $keys Main key used for the result diff (versions|columns)
     * @param array $ignoredColumns The columns to exclude from the diff.
     * @return array A list of differences
     * @throws VersionableException
     */
    public function compareVersion(int $versionNumber, string $keys = 'columns', array $ignoredColumns = []): array
    {
        $fromVersion = $this;
        $toVersion = $this->getOneVersion($versionNumber);
        if (is_null($toVersion)) {
            throw new VersionableException('Not existing version: '.$versionNumber);
        }

        return $this->computeDiff($fromVersion, $toVersion, $keys, $ignoredColumns);
    }

    /**
     * Compares two versions of the current object.
     * <code>
     * print_r($book->compareVersions(1, 2));
     * => array(
     *   '1' => array('Title' => 'Book title at version 1'),
     *   '2' => array('Title' => 'Book title at version 2')
     * );
     * </code>
     *
     * @param int $fromVersionNumber
     * @param int $toVersionNumber
     * @param string $keys Main key used for the result diff (versions|columns)
     * @param array $ignoredColumns The columns to exclude from the diff.
     * @return  array A list of differences
     * @throws VersionableException
     */
    public function compareVersions(int $fromVersionNumber, int $toVersionNumber, string $keys = 'columns', array $ignoredColumns = []): array
    {
        $fromVersion = $this->getOneVersion($fromVersionNumber);
        $toVersion = $this->getOneVersion($toVersionNumber);
        if (is_null($fromVersion)) {
            throw new VersionableException('Not existing version: '.$fromVersionNumber);
        }
        if (is_null($toVersion)) {
            throw new VersionableException('Not existing version: '.$toVersionNumber);
        }

        return $this->computeDiff($fromVersion, $toVersion, $keys, $ignoredColumns);
    }

    /**
     * Computes the diff between two versions.
     *
     * @param Model $fromVersion An object representing the original version.
     * @param Model $toVersion An object representing the destination version.
     * @param string $keys Main key used for the result diff (versions|columns).
     * @param array $ignoredColumns The columns to exclude from the diff.
     * @return  array A list of differences
     */
    protected function computeDiff(Model $fromVersion, Model $toVersion, string $keys = 'columns', array $ignoredColumns = []): array
    {
        $onlyColumns = $toVersion->getVersionableColumns();
        $fromVersionNumber = $fromVersion->getVersion();
        $toVersionNumber = $toVersion->getVersion();

        $ignoredColumns = array_unique(
            array_merge(
                $fromVersion->getIgnoredAttributes(), $toVersion->getIgnoredAttributes(), $ignoredColumns
            )
        );
        $diff = [];

        $fromVersionArray = $fromVersion->toArray();
        $toVersionArray = $toVersion->toArray();
        foreach ($fromVersionArray as $key => $value) {
            if (!in_array($key, $onlyColumns)) {
                continue;
            }
            if (in_array($key, $ignoredColumns)) {
                continue;
            }
            if ($toVersionArray[$key] != $value) {
                switch ($keys) {
                    case 'versions':
                        $diff[$fromVersionNumber][$key] = $value;
                        $diff[$toVersionNumber][$key] = $toVersionArray[$key];
                        break;
                    default:
                        $diff[$key] = array(
                            $fromVersionNumber => $value,
                            $toVersionNumber => $toVersionArray[$key],
                        );
                        break;
                }
            }
        }

        return $diff;
    }

    /**
     * retrieve the last $number versions.
     *
     * @param int $number the number of record to return.
     * @return Collection|Builder[] List of twPageVersion objects
     * @throws VersionableException
     */
    public function getLastVersions(int $number = 10): Collection
    {
        $object_name = $this->getVersionModelName();
        $version_obj = new $object_name();

        $v = $object_name::where($version_obj->getVersionableModelForeignKey(), $this->getKey())
            ->orderBy($version_obj->getVersionColumnName(), 'desc')
            ->limit($number);
        unset($version_obj);
        return $v->get();
    }

    public function getIgnoredAttributes(): array
    {
        $ignored = [$this->getVersionColumnName()];
        if ($this->isVersionCreatedAtUsed()) {
            $ignored[] = $this->getVersionCreatedAtColumnName();
        }
        if ($this->isVersionCreatedByUsed()) {
            $ignored[] = $this->getVersionCreatedByColumnName();
        }
        return $ignored;
    }
}