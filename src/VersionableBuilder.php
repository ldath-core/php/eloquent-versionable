<?php

namespace Thunderwolf\EloquentVersionable;

use Illuminate\Database\Eloquent\Builder;

class VersionableBuilder extends Builder
{
    use VersionableBuilderTrait;
}