<?php

namespace Thunderwolf\EloquentVersionable;

class VersionableVersionHelper extends VersionableHelper
{
    /**
     * Parsing columns given to
     *
     * @param array $columns
     * @return array
     * @throws VersionableException
     */
    public static function parseColumns(array $columns): array
    {
        $columns['version_model'] = static::class;  // Not in use - just to not have exception
        $columns = parent::parseColumns($columns);
        if (!array_key_exists('versionable_model', $columns)) {
            throw new VersionableException('Setting versionable_model is Required to know source Model');
        }
        if (!array_key_exists('versionable_model_foreign_key', $columns)) {
            $columns['versionable_model_foreign_key'] = 'id';
        }
        if (!array_key_exists('version_columns', $columns) && empty($columns['version_columns'])) {
            throw new VersionableException('Setting columns which will be versioned is Required');
        }

        return $columns;
    }
}