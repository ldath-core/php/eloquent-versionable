<?php

use Illuminate\Database\Capsule\Manager;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
use Thunderwolf\EloquentVersionable\VersionableServiceProvider;

include __DIR__.'/../vendor/autoload.php';

$container = new Container();
$provider = new VersionableServiceProvider($container);
$provider->register();

$capsule = new Manager($container);
$capsule->addConnection([ 'driver' => 'sqlite', 'database' => ':memory:', 'prefix' => 'prfx_' ]);
$capsule->setEventDispatcher(new Dispatcher);
$capsule->setAsGlobal();
$capsule->bootEloquent();

include __DIR__ . '/models/Book.php';
include __DIR__ . '/models/BookVersion.php';
include __DIR__ . '/models/BookDetails.php';
include __DIR__ . '/models/BookDetailsVersion.php';
include __DIR__ . '/models/BookVersioningNecessary.php';
include __DIR__ . '/models/BookVersioningNecessaryVersion.php';
