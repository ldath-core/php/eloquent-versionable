<?php

use Illuminate\Database\Eloquent\Model;
use Thunderwolf\EloquentVersionable\VersionableVersion;

class BookDetailsVersion extends Model
{
    use VersionableVersion;

    protected $table = 'book-details-version';

    protected $fillable = ['title'];  // Must be the same as we have in the `version_columns`

    public $timestamps = false;
    public $incrementing = false;

    public static function versionableVersion(): array
    {
        return [
            'version_columns' => ['title'],
            'versionable_model' => BookDetails::class,
            'log_created_at' => true,
            'log_created_by' => true,
            'log_comment' => true,
        ];
    }
}