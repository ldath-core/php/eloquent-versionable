<?php

use Illuminate\Database\Eloquent\Model;
use Thunderwolf\EloquentVersionable\Versionable;

class BookDetails extends Model
{
    use Versionable;

    protected $table = 'book-details';

    protected $fillable = ['title'];

    public $timestamps = false;

    public static function versionable(): array
    {
        return [
            'version_model' => BookDetailsVersion::class,
            'log_created_at' => true,
            'log_created_by' => true,
            'log_comment' => true,
        ];
    }
}