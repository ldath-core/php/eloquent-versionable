<?php

use Illuminate\Database\Eloquent\Model;
use Thunderwolf\EloquentVersionable\VersionableVersion;

class BookVersioningNecessaryVersion extends Model
{
    use VersionableVersion;

    protected $table = 'book-versioning-necessary-version';

    protected $fillable = ['title', 'isbn'];  // Must be the same as we have in the `version_columns`

    public $timestamps = false;
    public $incrementing = false;

    public static function versionableVersion(): array
    {
        return [
            'version_columns' => ['title'],
            'versionable_model' => BookVersioningNecessary::class
        ];
    }
}