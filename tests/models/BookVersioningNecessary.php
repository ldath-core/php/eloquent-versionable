<?php

use Illuminate\Database\Eloquent\Model;
use Thunderwolf\EloquentVersionable\Versionable;

class BookVersioningNecessary extends Model
{
    use Versionable {
        isVersioningNecessary as protected traitIsVersioningNecessary;
    }

    protected $table = 'book-versioning-necessary';

    protected $fillable = ['title', 'isbn'];

    public $timestamps = false;

    public function isVersioningNecessary(): bool
    {
        return $this->getAttribute('isbn') !== null && $this->traitIsVersioningNecessary();
    }

    public static function versionable(): array
    {
        return [
            'version_model' => BookVersioningNecessaryVersion::class
        ];
    }
}