<?php

use Illuminate\Database\Eloquent\Model;
use Thunderwolf\EloquentVersionable\Versionable;

class Book extends Model
{
    use Versionable;

    protected $table = 'book';

    protected $fillable = ['title'];

    public $timestamps = false;

    public static function versionable(): array
    {
        return [
            'version_model' => BookVersion::class
        ];
    }
}