<?php

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Carbon;
use PHPUnit\Framework\TestCase;
use Thunderwolf\EloquentVersionable\VersionableException;

class VersionableTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $schema = Manager::schema();

        $schema->dropIfExists('book');
        $schema->dropIfExists('book-version');
        $schema->dropIfExists('book-details');
        $schema->dropIfExists('book-details-version');
        $schema->dropIfExists('book-versioning-necessary');
        $schema->dropIfExists('book-versioning-necessary-version');

        Manager::connection()->disableQueryLog();

        $schema->create('book', function (Blueprint $table1) {
            $table1->increments('id');
            $table1->string('title');
            $table1->createVersionable(['version_model' => BookVersion::class]);
        });

        $schema->create('book-version', function (Blueprint $table2) {
            $table2->string('title');
            $table2->createVersionableVersion(['versionable_model' => Book::class, 'version_columns' => ['title']]);
        });

        $schema->create('book-details', function (Blueprint $table3) {
            $table3->increments('id');
            $table3->string('title');
            $table3->createVersionable([
                'version_model' => BookDetailsVersion::class,
                'log_created_at' => true,
                'log_created_by' => true,
                'log_comment' => true,
            ]);
        });

        $schema->create('book-details-version', function (Blueprint $table4) {
            $table4->string('title');
            $table4->createVersionableVersion([
                'versionable_model' => BookDetails::class, 'version_columns' => ['title'],
                'log_created_at' => true,
                'log_created_by' => true,
                'log_comment' => true,
            ]);
        });

        $schema->create('book-versioning-necessary', function (Blueprint $table5) {
            $table5->increments('id');
            $table5->string('title');
            $table5->string('isbn')->nullable();
            $table5->createVersionable(['version_model' => BookVersioningNecessaryVersion::class]);
        });

        $schema->create('book-versioning-necessary-version', function (Blueprint $table6) {
            $table6->string('title');
            $table6->string('isbn')->nullable();
            $table6->createVersionableVersion(['versionable_model' => BookVersioningNecessary::class, 'version_columns' => ['title', 'isbn']]);
        });

        Manager::connection()->enableQueryLog();
    }

    public function setUp(): void
    {
        $book_data = include __DIR__ . '/data/book.php';
        Manager::table('book')->insert($book_data);

        $book_version_data = include __DIR__ . '/data/book-version.php';
        Manager::table('book-version')->insert($book_version_data);

        $book_details_data = include __DIR__ . '/data/book-details.php';
        Manager::table('book-details')->insert($book_details_data);

        $book_details_version_data = include __DIR__ . '/data/book-details-version.php';
        Manager::table('book-details-version')->insert($book_details_version_data);

        $book_versioning_necessary_data = include __DIR__ . '/data/book-versioning-necessary.php';
        Manager::table('book-details')->insert($book_versioning_necessary_data);

        $book_versioning_necessary_version_data = include __DIR__ . '/data/book-versioning-necessary-version.php';
        Manager::table('book-details-version')->insert($book_versioning_necessary_version_data);

        Manager::connection()->flushQueryLog();

        date_default_timezone_set('Europe/Warsaw');
    }

    public function tearDown(): void
    {
        Manager::table('book')->truncate();
        Manager::table('book-version')->truncate();
        Manager::table('book-details')->truncate();
        Manager::table('book-details-version')->truncate();
        Manager::table('book-versioning-necessary')->truncate();
        Manager::table('book-versioning-necessary-version')->truncate();
    }

    public function testBaseVersioning()
    {
        $book = new Book();

        $book->setAttribute('title', 'War and Peas');
        $book->save();
        $this->assertEquals(1, $book->getVersion());

        $book->setAttribute('title', 'War and Peace');
        $book->save();
        $this->assertEquals(2, $book->getVersion());

        $book->toVersion(1);
        $this->assertEquals('War and Peas', $book->getAttribute('title'));
        $book->save();
        $this->assertEquals(3, $book->getVersion());

        $diff = $book->compareVersions(1, 2);
        $this->assertEquals('War and Peas', $diff['title'][1]);
        $this->assertEquals('War and Peace', $diff['title'][2]);

        $id = $book->getKey();
        $book->delete();

        $obj = Book::query()->find($id);
        $this->assertNull($obj);

        $collection = BookVersion::query()->where('id',$id)->get();
        $this->assertInstanceOf(Collection::class, $collection);
        $this->assertTrue($collection->isEmpty());
    }

    /**
     * @throws VersionableException
     */
    public function testDetailedVersioning()
    {
        $date1 = Carbon::now();
        $book = new BookDetails();
        $book->setAttribute('title', 'War and Peas');
        $book->setVersionCreatedBy('John Doe');
        $book->setVersionComment('Book creation');
        $book->save();

        $book->setAttribute('title', 'War and Peace');
        $book->setVersionCreatedBy('John Doe');
        $book->setVersionComment('Corrected typo on book title');
        $book->save();

        $book->toVersion(1);
        $this->assertEquals('John Doe', $book->getVersionCreatedBy());
        $this->assertEquals('Book creation', $book->getVersionComment());
        $this->assertEqualsWithDelta($date1->timestamp, $book->getVersionCreatedAt()->timestamp, 1);


        $allVersions = $book->getAllVersions();
        $this->assertInstanceOf(Collection::class, $allVersions);
        $expected = [
            ['War and Peas', 1, 'John Doe', 'Book creation'],
            ['War and Peace', 2, 'John Doe', 'Corrected typo on book title']
        ];
        foreach ($allVersions as $bookVersion) {
            $current = array_shift($expected);
            $this->assertEquals(
                $current,
                [
                    $bookVersion->getAttribute('title'),
                    $bookVersion->getVersion(),
                    $bookVersion->getVersionCreatedBy(),
                    $bookVersion->getVersionComment()
                ]
            );
        }
    }

    public function testConditionalVersioning()
    {
        $book = new BookVersioningNecessary();
        $book->setAttribute('title', 'War and Peas');
        $book->save(); // book is saved, no new version is created
        $collection = BookVersioningNecessaryVersion::query()->where('id',$book->getKey())->get();
        $this->assertInstanceOf(Collection::class, $collection);
        $this->assertTrue($collection->isEmpty());

        $book->setAttribute('isbn', '0553213105');
        $book->save(); // book is saved, and a new version is created
        $collection = BookVersioningNecessaryVersion::query()->where('id',$book->getKey())->get();
        $this->assertInstanceOf(Collection::class, $collection);
        $this->assertFalse($collection->isEmpty());

        Book::disableVersioning();
        $book = new Book();
        $book->setAttribute('title', 'Pride and Prejudice');
        $book->setVersion(1);

        $book->save();
        // book is saved, no new version is created
        $collection = BookVersion::query()->where('id',$book->getKey())->get();
        $this->assertInstanceOf(Collection::class, $collection);
        $this->assertTrue($collection->isEmpty());

        $book->addVersion();
        // a new version is created
        $collection = BookVersion::query()->where('id',$book->getKey())->get();
        $this->assertInstanceOf(Collection::class, $collection);
        $this->assertFalse($collection->isEmpty());

        // you can re-enable versioning using the Query static method enableVersioning()
        Book::enableVersioning();
        $book->setAttribute('title', 'Pride and Prejudice - NEW');
        $book->save();
        $this->assertEquals(2, $book->getVersion());
        $collection = BookVersion::query()->where('id',$book->getKey())->get();
        $this->assertInstanceOf(Collection::class, $collection);
        $this->assertFalse($collection->isEmpty());
        $this->assertEquals(2, $collection->count());
    }
}